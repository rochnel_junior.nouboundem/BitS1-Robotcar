# Base Packages

Code und Bibliotheken für das Bits1 Fahrzeug 

# Beschreibung

"Robotcar-BitS1"-Code angepasst auf thk-Bibliotheken.
Bibliotheken "DeviceDriverSet_xxx0" und "MPU6050_getdata" nicht mehr notwendig.

# Vorraussetzung

- Bits1 Fahrzeug

- [thk-BITS1-Bibliotheken](https://git-ce.rwth-aachen.de/bits_libs/bits1-libraries) Bibliothek


# Installation

1. Kopiere die Bibliotheken aus base-package in den Arduino libraries Ordner. 

2. Umbennen der .ino Datei analog zu dem des Verzeichnisses.

3. Hochladen der .ino Datei auf das Arduino Uno Board des Bits1 Fahrzeugs.


# Anwendung

1. Bits Fahrzeug einschalten

2. Fahrzeug auf der Teststrecke platzieren

3. Fahrmodi wählen: LED Fahrmodi: grün = Line Follow, blau = Obstacle Avoidance

# Anmerkungen
- Bibliotheks Ordnerstruktur: Arduino\libaries

- Programm Ordnerstruktur: Arduino\Robotcar-BitS1

**Achtet darauf, dass der Ordner in dem sich die .ino Datei befindet den selben Namen hat wie die .ino Datei**

- Das Fahrzeug stoppt wenn es angehoben wird. 



            


