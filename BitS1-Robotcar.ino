
/***************************************************
   ________  ___  _________  ________    _____
  |\   __  \|\  \|\___   ___\\   ____\  / __  \
  \ \  \|\ /\ \  \|___ \  \_\ \  \___|_|\/_|\  \
   \ \   __  \ \  \   \ \  \ \ \_____  \|/ \ \  \
    \ \  \|\  \ \  \   \ \  \ \|____|\  \   \ \  \
     \ \_______\ \__\   \ \__\  ____\_\  \   \ \__\
      \|_______|\|__|    \|__| |\_________\   \|__|
                                \|_________|
  Name:
  BitS1 v2

  Author:
  Prof. Kamau
  Jannis Kampmann
  IFK TH Köln

**************************************************/
#include <FastLED.h>
#include <thk_imu.h>
#include <thk_motor_driver.h>
#include <thk_motor_driver_tb6612fng.h>
#include "Robotcar_functions.h"
Robotcar_functions Robotcar_functions_object;

//Definieren von Variablen
float turn_factor = 1.0;
const float distance_one = 0;
const float distance_two = 0;
int acceleration_increment = 0;
int total_velocity = 0;
int white_surface_reflection = 150;

void setup() 
{ //Innerhalb dieser Funktion wird alles einmal bei Start des Arduinos aufgerufen
  Robotcar_functions_object.initialise();
  search_time = 1.5 * total_velocity;
}

void loop() 
{ //Alles innerhalb dieser Funktion wird so lange wiederholt, wie der Arduino eingeschaltet ist  
  Robotcar_functions_object.serial_sensor_data_output(); 
  Robotcar_functions_object.mode_selection(); 
  Robotcar_functions_object.check_standby(); 
  Robotcar_functions_object.car_on_ground();  
  Robotcar_functions_object.check_voltage();
  Robotcar_functions_object.obstacle_avoidance();
  Robotcar_functions_object.line_follow();  
}
